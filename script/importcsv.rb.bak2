#!/usr/bin/env ruby     
require File.dirname(__FILE__)+'/../config/environment'

FIELDS = %q(Confirmation,Date,Start Time,End Time,Time Zone,Group,First Name,Last Name,Business Name,Daytime Phone,Evening Phone,Mobile Phone,E-mail).split(',')

# -------------------------------------

def log(msg)
  puts msg
end

def find_affiliate(affiliate_name)
  affiliate = Affiliate.find_by_name(affiliate_name)
  unless affiliate
    log("  ERROR:  Unable to find an affiliate using the name '#{affiliate_name}'")
    existing_affiliates = Affiliate.find(:all).map {|a| a.name}.join(', ')
    log("  Existing affiliates: #{existing_affiliates}")
    exit(-1)
  else
    log("  Found affiliate #{affiliate.name} : #{affiliate.email}")
  end    
  affiliate
end

def get_user(fields)    
  email = fields[12]
  user = User.find_by_email(email)
  log("    Found existing user for #{email}") if user
  user
end

def create_user(fields)
  user = User.new
  user.first_name = fields[6]
  user.last_name = fields[7]
  user.email = fields[12]
  user.affiliate_id = $affiliate.id
  if user.save
    log("    Created user #{user.full_name} : #{user.email}")
  else    
    log("    ERROR:  Unable to save user #{user.full_name}: #{format_errors(user.errors)}")
    user = nil
  end
  user
end

def create_appointment(fields, user) 
  apt = Appointment.new
  apt.token = fields[0]
  apt.date = fields[1]  
  apt.start_time = fields[2]
  apt.end_time = fields[3]
  apt.time_zone = fields[4] 
  apt.user_id = user.id    
  if apt.save     
    log("    Created appointment ##{apt.token} : #{apt.start_time_t}")
  else
    log("    ERROR:  Unable to save apt\n#{format_errors(apt.errors)}")
    apt = nil
  end
  apt
end
   
def past?(apt, window = 0.seconds) 
  apt.start_time_t + window < Time.now
end

def get_appointment(fields)
  apt = Appointment.find_by_token(fields[0])                    
  if apt
    log("    WARNING: Found existing appointment in the database: ##{fields[0]}.")
    log("    WARNING: Skipping this appointment.  It's already in the system")
  end
  apt
end

def send_invite(apt)  
  email = Mailer.create_invite_mom(apt)  
  email.set_content_type("text/html") 
  log "    Sending new apt. email to #{apt.user.full_name} (#{email.to})"
  Mailer.deliver(email)
end        

def validate_fields(fields)   
  if fields.size < FIELDS.size
    log("  ** Error: Did not parse the first line of fields.  ")
    log("  ** Expected:")
    log("  #{FIELDS.join(',')}")
    log("  ** Found   :")
    log("  #{fields.join(',')}")
  end
  FIELDS.each_with_index do |field, i|
    found, expected = FIELDS[i], fields[i]
    if found != expected
      log("  ** Error:  Error parsing first line.  Expected to find #{expected}. Found #{found} instead.")
      exit(-1)
    end
  end
end

def format_errors(errors) 
  errors.full_messages.join(',')
end


def usage()
  log <<-END
  
  Usage:  ruby importcsv.rb ["Affiliate name"] [filename]
    filename - the name of the CSV import file
    Affiliate name - the name of the affiliate.  Must be in quotes
    END
  exit(-1)
end

# -------------------------------------
# Main
# -------------------------------------

if $0 == __FILE__:

  # find the affiliate     
  if ARGV.size >= 1
    $affiliate = find_affiliate(ARGV[0])
    $user_count_start = $affiliate.users.count
    $apt_count_start = $affiliate.appointments.count
  end

  # check the arguements
  usage() unless ARGV.size == 2

  # read the file
  csv_filename = ARGV[1]
  csv_lines = nil
  begin
    File.open(csv_filename, "r") {|f| csv_lines = f.readlines}  
    log("  Read #{csv_lines.size} lines from #{csv_filename}")
  rescue Exception => e
    log("**Error reading #{csv_filename}: #{e.to_s}")
    exit(-1)
  end            
  
  # strip the first line
  first_line = csv_lines[0].strip
  fields = first_line.split(',')
  validate_fields(fields)
  
  # parse the remainder
  data_lines = csv_lines[1..-1]     
  data_lines.each_with_index do |line, index|
    # skip the blank line
    next unless line.strip.size > 0

    # get the first line, and validate
    fields = line.strip.split(',')

    # get an existing user or create a new one
    log("  Parsing entry #{index}")
    user = get_user(fields)
    unless user
      new_user = true
      user = create_user(fields)
    else
      new_user = false
    end           

    # get the appointment
    if user
      apt = get_appointment(fields)
      # do not send invites for existing appointments
      unless apt
        apt = create_appointment(fields, user) 
        if past?(apt) 
          log("    WARNING:  Appointment is past due.  Removing appointment from database")
          apt.destroy
          if new_user
            log("    WARNING:  Removing user that was just added: #{user.email}")
            user.destroy
          end
        else
          send_invite(apt) if apt
        end
      end
    end
  end

  $user_count_end = $affiliate.users.count
  $apt_count_end = $affiliate.appointments.count    
  log("\n")
  log("Summary:")
  log("  Parsed #{data_lines.size} entries")
  log("  Added #{$user_count_end-$user_count_start} users for #{$affiliate.name}")
  log("  Added #{$apt_count_end-$apt_count_start} appointments for #{$affiliate.name}")
  
end


