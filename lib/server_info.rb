require 'rubygems'
require 'rexml/document'

# FYI, this weird structure makes the original structure used from Windows Media SDK

class ServerInfo
  attr_accessor :name, :status

  def self.server_infos
    xml = request_wowza_xml

    doc = REXML::Document.new(xml)
    results = []
    doc.elements.each("//Application[Name='live']/ApplicationInstance/Stream/Name") do |e|
      info = ServerInfo.new
      info.name = e.text
      info.status = 'Started'
      results << info
    end

    names = results.map {|server_info| server_info.name }
    puts "  Found the following live streams: #{names.join(', ')}"

    results
  end

  def self.request_wowza_xml
    # use wget and spit to the command line
    url = application_settings["wowza"]["url"]
    puts "Making XML request to #{url}"
    response = `wget -qO- #{url}` 
    puts "  Got response #{response}" if response.size > 0
    response
  end

end


