require 'constants'

class Affiliate < ActiveRecord::Base

  include Constants 

  has_many :users
  has_many :appointments, :through => :users
  has_many :purchases
  
  # affiliate ->* users ->* appointments ->* purchases *<- users
  def purchases_by_users
    self.appointments(true).inject([]) do |array, apt|
      apt.purchases.each {|p| array << p }
      array
    end
  end
  
  def purchased
    self.purchases_by_users.select {|p| p.purchased_on != nil }
  end

  def <=>(other)
    self.name <=> other.name
  end

  validates_presence_of :name, :active, :is_admin, :email, :password
  validates_uniqueness_of :name, :email
  validates_format_of :is_admin, :with  => TRUE_FALSE_REGEX, :message => "should be 0 or 1" 
  validates_format_of :active, :with  => TRUE_FALSE_REGEX, :message => "should be 0 or 1" 
  validates_format_of :email, :with  => EMAIL_REGEX, :message  => "is not a valid email format"

end

