# gem install will_paginate -v 2.3.16
require 'will_paginate'

class AdminController < ApplicationController

  before_filter :authorize, :except => [:login, :forgot_password, :send_password] 
  before_filter :verify_affiliate, :only => [:edit_profile, :update_profile, :view_profile,
    :affiliate, :appointments, :purchases] 

  layout "user"

  # --------------------------------------------

  def forgot_password
  end
  
  def send_password 
    # todo refactor with the user/send_password
    email = params[:forgot][:email].downcase
    found = Affiliate.find_by_email(email)
    if found
      flash[:notice] = "Your password has been mailed to #{email}"
      # send the email
      email = Mailer.create_forgot_password(found, request, 'admin')  
      Mailer.deliver(email)
      redirect_to :action => "login"
    else
      flash[:notice] = "Unable to find an affiliate with this email address: #{params[:forgot][:email]}"
      redirect_to :action => "forgot_password"
    end
  end       
  
  # --------------------------------------------
  
  # todo refactor with user login
  def login
    # new attempt
    if request.get? 
      session[:affiliate_id] = nil
      @login = Affiliate.new
      # find the name to populate the email form
      affiliate = Affiliate.find_by_id(params[:id])
      @address = (affiliate.nil?) ? "" : affiliate.email
    else                                      
      unverified = Affiliate.new(params[:login])
      affiliate = unverified.login
      # valid attempt
      if affiliate
        session[:affiliate_id] = affiliate.id
        flash[:notice] = "#{affiliate.email} logged in" 
        # go to the url specified in the page parameter if it's present
        if params[:page] != nil
          redirect_to params[:page]
        else 
          redirect_to :action => 'index', :id => affiliate.id
        end
      # invalid attempt
      else
        flash.now[:notice] = "Invalid user / password combination" 
        logger.info "[ALERT] Friend Login attempt failed.  Email=#{unverified.email} Password=#{unverified.password} IP=#{request.remote_ip}"
        render :action => 'login', :page => params[:page]   
      end
    end
  end     
  
  def logout
    flash[:notice] = "#{@current.name} logged off."
    session[:affiliate_id] = nil
    redirect_to :action => "login"
  end
  
  # --------------------------------------------

  def index
  end
  
  def edit_profile   
  end
     
  def update_profile
    if @affiliate.update_attributes(params[:affiliate])
      flash[:notice] = "#{@affiliate.name}'s has been updated."
      redirect_to :action => "index", :id => @current.id
    else        
      render :action => :edit_profile, :id => @affiliate.id 
    end
  end   
  
  def affiliates    
    @affiliates = Affiliate.paginate(:page => params[:page], :per_page => 20, :order => 'name')
  end      
  
  def affiliate
  end

  def create_affiliate
    if request.post?
      @affiliate = Affiliate.new(params[:affiliate])
      if @affiliate.save
        flash[:notice] = "Affiliate #{@affiliate.name} has been created"
        redirect_to :action => "affiliate", :id => @affiliate.id
      end
    end
  end
  
  # --------------------------------------------

  def all_appointments      
    @title = "Appointments"
    @appointments = Appointment.paginate :page => params[:page], :per_page => 20, :order => 'date desc'
    render :action => 'appointments'
  end

  # view appointments for a specific affiliate
  def appointments
    @title = "Appointments for #{@affiliate.name}"
    # have to SQL sort on created_on.. the sort on "date" is a string sort
    tmp_appointments = @affiliate.appointments.sort { |apt1, apt2| Date.parse(apt2.date) <=> Date.parse(apt1.date) }.reverse
    @appointments = tmp_appointments.paginate :page => params[:page], :per_page => 20
  end

  # view a specific appointment
  def appointment
    @appointment = Appointment.find_by_id(params[:id])
    display_error "Unable to find appointment #{params[:id]}" unless @appointment
  end

  # renders the forms and also the results of they are posting
  def find_appointment
    if request.post?
      results = []
      case params[:by]
      when "email"
        user = User.find_by_email(params[:user][:email].strip)
        results = user.appointments(true) if user
      when "token"
        apt = Appointment.find_by_token(params[:appointment][:token].strip)
        results << apt if apt
      when "date"
        date = get_date_string(params[:date])
        logger.debug "** Searching using date: #{date}"
        results = Appointment.find_all_by_date(date)
      end

      # filter the results depending on if the user is an admin.  
      # TODO:  this is unoptimized.  we could filter via a DB query not in memory like this.
      filtered_results = filter_find_result(results)

      # FYI, just show them all, otherwise
      @appointments = filtered_results.paginate :page => params[:page], :per_page => 1000

      unless @appointments && @appointments.size > 0
        case params[:by]
        when "email": flash.now[:notice] = "Unable to find a user that matches this email: #{params[:user][:email]}"
        when "token": flash.now[:notice] = "Unable to find an appointment with this token: #{params[:appointment][:token]}"
        when "date":  flash.now[:notice] = "Unable to find an appointment with that date."
        end
      end
    end
  end
  
  def find_affiliates_paid
    if request.post?
      @from_date = Date.parse("#{params[:purchase]['from(1i)']}-#{params[:purchase]['from(2i)']}-#{params[:purchase]['from(3i)']}")
      @to_date = Date.parse("#{params[:purchase]['to(1i)']}-#{params[:purchase]['to(2i)']}-#{params[:purchase]['to(3i)']}")
      @affiliates = []
      if params[:purchase][:affiliate] == ''
        @affiliates = Affiliate.all
      else
        @affiliates << Affiliate.find(params[:purchase][:affiliate])
      end
    end
  end
  
  def add_appointment
    @appointment = Appointment.new
    get_time_array()
  end
  
  def create_appointment
    @appointment = Appointment.new
    @mommy = User.find_by_email(params[:mommy][:email])
    
    # create user
    unless @mommy
      new_user = true
      @mommy = User.new
      @mommy.first_name = params[:mommy][:first_name]
      @mommy.last_name = params[:mommy][:last_name]
      @mommy.email = params[:mommy][:email]
      if @current.is_admin == 1
        @mommy.affiliate_id = params[:mommy][:affiliate]
      else
        @mommy.affiliate_id = @current.id
      end
      if !@mommy.save
        @mommy.errors.each {|attr, msg| @appointment.errors.add("user", "'s #{attr} #{msg}")}
        get_time_array()
        render :action => "add_appointment"
        return
      end
    else
      new_user = false
    end
    
    # create appointment
    @appointment.token = params[:appointment][:email]
    @appointment.date = Date.parse("#{params[:appointment]['date(1i)']}-#{params[:appointment]['date(2i)']}-#{params[:appointment]['date(3i)']}").strftime("%m/%d/%Y")
    @appointment.start_time = params[:appointment][:start_time]
    @appointment.end_time = params[:appointment][:end_time]
    @appointment.time_zone = params[:appointment][:time_zone]
    @appointment.token = params[:appointment][:token]
    @appointment.user_id = @mommy.id
    
    if !@appointment.save
      get_time_array()
      @appointment.date = Date.today + 1
      render :action => "add_appointment"
      return
    end
    
    if @appointment.start_time_t + 0 < Time.now
      @appointment.destroy
      if new_user
        @mommy.destroy
      end
      flash[:notice] = "WARNING: Appointment is past due. Appointment is removed from database."
    else
      send_new_appointment_email(@appointment) if @appointment
      flash[:notice] = "New appointment is added and invitation email is sent."
    end
    
    
    if @current.is_admin == 1
      redirect_to :action => "index", :id => @current.id
    else
      redirect_to :action => "index", :id => @current.id
    end
  end
  
  def purchase
    @appointment = Appointment.find_by_id(params[:apt_id])
    @appointment.user.purchase(@appointment, @current.id)
    flash[:notice] = "Appointment is purchased by you. User can access this appointment."
    redirect_to :action => "appointments", :id => @current.id
  end
  # --------------------------------------------

  def edit_user_email           
    @appointment = Appointment.find(params[:id])
    @user = @appointment.user
    if request.post?
      if @user.update_attributes(params[:user])
        flash[:notice] = "User's email has been updated."
        redirect_to :action => "appointment", :id => @appointment.id
      else        
        render :action => :edit_user_email, :id => @appointment.id # yea, I know it's the apt id
      end
    end
  end


  # --------------------------------------------

  def view_archive
    @appointment = Appointment.find(params[:id])
    if @appointment
      @video_url = @appointment.archive_url
    else
      display_error "Unable to find appointment #{params[:id]}" 
    end
  end
  
  # --------------------------------------------
  
  def view_purchase
    @purchase = Purchase.find(params[:id])
  end
  
  def purchases
    @title = "Purchases for #{@affiliate.name}"
    @purchases = @affiliate.purchased.paginate(:page => params[:page], :per_page => 20)
    render :action => "purchases"    
  end

  def all_purchases
    @title = "All Purchases"
    @purchases = Purchase.paginate(:page => params[:page], :per_page => 20, 
      :conditions => ["purchased_on is not NULL"])
    render :action => "purchases"
  end                        
  
  def remove_purchase           
    purchase = Purchase.find_by_id(params[:id])
    if purchase
      purchase.destroy
      flash[:notice] = "Purchase by #{purchase.friend.nil? ? purchase.user.email : purchase.friend.email} has been deleted from the database"
    else
      flash[:notice] = "Error.  Unable to find this purchase in the database"
    end
    redirect_to :action => "all_purchases"
  end
  
  def resend_invitation     
    @appointment = Appointment.find(params[:id]) 
    send_new_appointment_email(@appointment)
    flash[:notice] = "Invitation email has been resent to #{@appointment.user.email}" 
    redirect_to :action => "appointment", :id => @appointment.id
  end

  # --------------------------------------------
  
  private       

  # NOTE:  copied from lib/appointment_find.rb
  def send_new_appointment_email(apt)
    # send it to the mom
    email = Mailer.create_invite_mom(apt)  
    email.set_content_type("text/html") 
    Mailer.deliver(email)
  end

  def get_date_string(d)
    # d = {"month"=>"5", "day"=>"8", "year"=>"2007"}
    day = pad_with_zeroes d["day"]
    month = pad_with_zeroes d["month"]
    year = d["year"]
    "#{month}/#{day}/#{year}"  # 05/08/2007
  end
  
  # convert "5" to "05" but not "12"
  def pad_with_zeroes(string)
    (string.size == 1) ? "0#{string}" : string
  end
    
  
  # if you are not an admin, do not display find results for appointments that don't belong to you
  # TODO:  rework this to use apply a dynamic database scope in stead of filtering in memory
  def filter_find_result(appointments)
    if @current.is_admin == 1
      appointments
    else
      appointments.select {|apt| apt.user.affiliate.id == @current.id }
    end
  end
  
  def locate_appointments_by_date(d)
    # d = {"month"=>"5", "day"=>"20", "year"=>"2007"}
    date = "#{d["month"]}/#{d["day"]}/#{d["year"]}"
  end
  
  # def display_appointments(appointments)       
  #   # TODO:  render by start date grouped by affiliate names?
  #   @appointments = Appointment.paginate(:page => params[:page], :per_page => 20)
  #   render :action => "appointments"
  # end

  def handle_appointment_sorting(appointments)
    # sort by the affiliate name, subsort by the start time    
    appointments.sort do |apt1, apt2|
      if apt1.user.affiliate.name == apt2.user.affiliate.name
        apt2.start_time_t <=> apt1.start_time_t  # reverse chronological
      else
        apt1.user.affiliate.name <=> apt2.user.affiliate.name # sort by affiliate name first
      end
    end         
  end
  
  def handle_purchase_sorting(purchases)
    # sort by the affiliate name, subsort by the start time    
    purchases.sort do |pur1, pur2|
      if pur1.appointment.user.affiliate.name == pur1.appointment.user.affiliate.name
        pur2.purchase_date <=> pur1.purchase_date  # reverse chronological
      else
        pur1.appointment.user.affiliate.name <=> pur2.appointment.user.affiliate.name # sort by affiliate name first
      end
    end         
  end

  def verify_affiliate
    @affiliate = Affiliate.find_by_id(params[:id])
    display_error "Unable to find affiliate #{params[:id]}" unless @affiliate
  end      
  
  def authorize
    unless session[:affiliate_id]
      flash[:notice] = "Please login before accessing that page"
      redirect_to :action  => 'login', :page => request.request_uri     
    else
      @current = Affiliate.find(session[:affiliate_id])
    end
  end      
  
  def get_time_array
    @time_array = []
    (0..23).each do |hour|
      (0..3).each do |quater|
        @time_array << Time.gm(2012, 9, 8, hour, quater * 15, 0).strftime("%I:%M %p")
      end
    end
  end
end
