class Mailer < ActionMailer::Base

  def forgot_password(who, request, controller, sent_at = Time.now)
    @subject    = "The Sonostream login information you requested"
    @recipients = "#{who.email}"  
    @from       = application_settings["support"]["email_address"]
    @sent_on    = sent_at
    @headers    = {}
    @body["user"] = who 
    @body["request"] = request
    @body["controller"] = controller 
    @body["site_url"] = application_settings["site"]["url"]         
  end 
   
  # --------------------------------------------
  
  def invite_friend(user, friend, apt, sent_at = Time.now)
    subject    = "#{user.full_name} has invited you to view her ultrasound!"
    handle_friend_email(subject, user, friend, apt, sent_at)
  end    

  def canceled_appointment(user, friend, apt, sent_at = Time.now)
    subject    = "#{user.full_name} has canceled her ultrasound appointment!"
    handle_friend_email(subject, user, friend, apt, sent_at)
  end  
  
  def rescheduled_appointment(user, friend, apt, sent_at = Time.now)
    subject    = "#{user.full_name} has rescheduled her ultrasound appointment!"
    handle_friend_email(subject, user, friend, apt, sent_at)
  end             
  
  def handle_friend_email(subject, user, friend, apt, sent_at)
    support_email =  application_settings["support"]["email_address"]
    @subject = subject
    # don't send the development mails to the real user
    @recipients = get_email(friend)
    @from       = support_email
    @headers["reply-to"] = "#{user.email}"
    @sent_on    = sent_at
    @headers    = {}
    @body["user"] = user 
    @body["friend"] = friend
    @body["apt"] = apt
    @body["site_url"] = application_settings["site"]["url"]     
  end    
  
  def admin_email(subject, msg, sent_from, sent_at=Time.now)
    support_email =  application_settings["support"]["email_address"]
    @recipients = support_email
    @from       = support_email
    @subject = "[SonostreamLive Admin] #{subject}"
    @body["message"] = msg
    @body["sent_from"] = sent_from
  end

  def get_email(user)
    # only send the real email if it's specifically disabled
    if application_settings["email"]["testing"] == 0
      "#{user.email}"
    else             
      application_settings["email"]["test_email"]
    end
  end 
  
  # --------------------------------------------
  
  def invite_mom(apt, sent_at = Time.now)   
    user = apt.user
    support_email =  application_settings["support"]["email_address"]
    @subject    = "#{user.first_name} - Share your 3D/4D ultrasound live with Sonostreamlive.net"
    @recipients = get_email(user)
    @from       = support_email
    @headers["reply-to"] = "#{support_email}"
    @sent_on    = sent_at
    @headers    = {}
    @body["user"] = user 
    @body["apt"] = apt
    @body["site_url"] = application_settings["site"]["url"]     
  end

  def purchase_mom(apt, template_num, sent_at = Time.now)
    user = apt.user
    support_email =  application_settings["support"]["email_address"]
    @subject    = "#{user.first_name} - A digital copy of your Ultrasound is available for your computer or smart phone"
    @recipients = get_email(user)
    @from       = support_email
    @headers["reply-to"] = "#{support_email}"
    @sent_on    = sent_at
    @headers    = {}
    @body["user"] = user 
    @body["apt"] = apt
    @body["site_url"] = application_settings["site"]["url"]
    @template = "purchase_mom_#{template_num}"
  end
  
  
end
