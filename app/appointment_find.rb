require 'rubygems'  
require 'active_record'

require 'net/pop'
require 'yaml'
require 'logger'         

require File.dirname(__FILE__) + '/../config/environment'

# --------------------------------------------

def assert(condition,msg="")
  raise Assertion.new(msg) unless condition
end

class Assertion < Exception; end        

def log(msg)
   $parser_log ||= Logger.new(File.dirname(__FILE__) + '/../log/parser.log')
   $parser_log.info(msg)
   puts msg if $verbose
end

# helper method for sending admin emails with error messages
def send_admin_email_helper(subject, message, sent_from, data=nil) 
  msg = "#{message}"
  msg += "\n\nData:    #{data.inspect}" if data
  e = Mailer.create_admin_email(subject, msg, sent_from)              
  Mailer.deliver(e)    
end

# --------------------------------------------

class AppointmentFind  
  
  RE_NAME  = /Name: (.+?) (.+?) /
  RE_DATE  = /Date: (\d+\/\d+\/\d+) /
  RE_TIME  = /Time: (\d+:\d+ [AP]M) - (\d+:\d+ [AP]M) (\w+) /
  RE_CONF  = /Confirmation #: (\d+) /
  RE_EMAIL = /E-mail: (.+?@.+?) /
  RE_BUSINESS = /Business: (.+?)=*\s*\**CUSTOMER\** /
  RE_NEW   = /(NEW APPOINTMENT)/
  RE_RESCHEDULE = /(APPOINTMENT RESCHEDULED)/
  RE_CANCEL = /(CANCELLATION REQUESTED)/ 
           
  def initialize
    @emails = []
  end

  def scrub!(email)
    email.gsub!(/=\r*\n*/,'') # HTML emails are line terminated with =\n, this can cut through words
    email.gsub!(/<.+?>/,'') # remove any tags 
    email.gsub!(/\n/,' ')  # but text emails, have regular newlines replace newlines with ' '
    email.gsub!(/&nbsp;/, ' ') # replace html spaces with regular spaces
    email.gsub!(/\s+/,' ')  # replace multiple spaces with just one 
  end

  def parse(email) 
    if email && email.size > 0     
      scrub!(email)
      result = {}
      # customer data
      parse_email(result, email, RE_NAME, :first_name, :last_name)
      parse_email(result, email, RE_EMAIL, :email)
      # affiliate data
      parse_email(result, email, RE_BUSINESS, :business)
      # appointment data
      parse_email(result, email, RE_DATE, :date)
      parse_email(result, email, RE_TIME, :start_time, :end_time, :time_zone)
      parse_email(result, email, RE_CONF, :token)
      parse_email(result, email, RE_NEW, :is_new)
      parse_email(result, email, RE_CANCEL, :is_cancellation)
      parse_email(result, email, RE_RESCHEDULE, :is_reschedule)
      result
    end
  end 
  
  def parse_email(result, email, re_pattern, *params)  
    params.each_with_index {|param,i| result[param] = eval("$#{i+1}") } if email =~ re_pattern
  end
  
  def get_emails(delete_from_mailbox=true)
    config_file = File.dirname(__FILE__) + '/../config/parser.yml'
    begin
      config = YAML::load(File.open(config_file))
      server, username, password = config["appointment_email"]["server"], config["appointment_email"]["username"], config["appointment_email"]["password"]
    rescue
      log "[Error] Error opening configuration file #{config_file}"
      raise
    end
    begin
      pop = Net::POP3.new(server)
      pop.start(username, password)
      log "Found #{pop.mails.size} emails on #{server} for #{username}"
      unless pop.mails.empty?
        pop.each_mail do |mail|
          @emails << mail.pop
          mail.delete if delete_from_mailbox
        end
      end   
      pop.finish
    rescue
      log "[Error] Error accessing POP3 account #{username} at #{server}"
      raise
    end
  end

  def process(environment, delete_from_mailbox=true, save_email=false)
    now = Time.now
    # initialize database
    conf = YAML::load(File.open(File.dirname(__FILE__) + '/../config/database.yml'))
    ActiveRecord::Base.establish_connection(conf[environment])
    # read the emails
    get_emails(delete_from_mailbox)  
    i = 0
    @emails.each do |email|
      i = i + 1

      File.open(File.dirname(__FILE__) + "/../test/emails/test_#{i}.eml","w+") { |f| f.write(email)} if save_email
      data = parse(email)
      File.open(File.dirname(__FILE__) + "/../test/emails/test_parsed_#{i}.eml","w+") { |f| f.write(email)} if save_email

      # skip spam emails, or emails that do not contain any parsed data
      if data.empty?
        log("Skipping invalid email.")
      else
        log("Processing parsed email for #{data[:first_name]} #{data[:last_name]}")
        process_data(data) unless data.empty? 
      end
    end                    
    return true
  end 
  
  def emails
    @emails
  end

  def process_data(data)
    if data[:is_new]
      apt = process_new_appointment(data)
      send_new_appointment_email(apt) if apt and apt.rescheduled == 0
    elsif data[:is_reschedule]
      apt = process_rescheduled_appointment(data)  
      send_rescheduled_appointment_emails(apt) if apt
    elsif data[:is_cancellation] 
      apt = process_canceled_appointment(data) 
      send_canceled_appointment_emails(apt) if apt
    end     
    apt
  end             
  
  def process_new_appointment(data)
    affiliate = find_affiliate(data)
    if affiliate  
      user = get_user(data, affiliate) # gets an existing user or creates the user
      if user
        user.affiliate_id = affiliate.id        
        user.save
        appointment = get_appointment(data)
        appointment.user_id = user.id
        appointment.save           
        return appointment
      end
    end   
    # an appointment is not processed if the affiliate can not be found in the database!
    return nil
  end          
  
  def process_rescheduled_appointment(data)
    apt = find_appointment(data)  
    if apt
      apt.start_time = data[:start_time]
      apt.end_time = data[:end_time]
      apt.date = data[:date]   
      apt.time_zone = data[:time_zone] 
      apt.rescheduled = 1
      apt.cancelled = 0
      apt.save      
      log("  Rescheduled appointment for #{apt.user.full_name}. Appointment had #{apt.friends.size} invited friends")  
    else
      # if the appointment is not in the system, treat it like a new appointment
      apt = process_new_appointment(data)
    end   
    apt
  end
       
  def process_canceled_appointment(data)
    apt = find_appointment(data)  
    if apt
      apt.cancelled = 1
      apt.save  
      log("  Canceled appointment for #{apt.user.full_name}. Appointment had #{apt.friends.size} invited friends")  
    else 
      log("[Error] Can't find appointment.  Unable to cancel appointment for #{data.inspect}")
    end    
    apt
  end
                            
  # --------------------------------------------
  
  def send_new_appointment_email(apt)
    # send it to the mom
    email = Mailer.create_invite_mom(apt)  
    email.set_content_type("text/html") 
    log "  Sending new apt. email to #{apt.user.full_name} (#{email.to})"
    Mailer.deliver(email)
  end
  
  def send_rescheduled_appointment_emails(apt)
    # mail out a reschedule email to all the friends who were invited to this email
    apt.friends.each do |friend|
      email = Mailer.create_rescheduled_appointment(apt.user, friend, apt)  
      email.set_content_type("text/html")
      log "  Sending rescheduled apt. (##{apt.token}) email to #{email.to[0]}"
      Mailer.deliver(email)
    end 
    apt.friends.size
  end
  
  def send_canceled_appointment_emails(apt)
    # mail out a cancelation email to all the friends who were invited to this email
    apt.friends.each do |friend|
      email = Mailer.create_canceled_appointment(apt.user, friend, apt)  
      email.set_content_type("text/html")
      log "  Sending canceled apt. (##{apt.token}) email to #{email.to[0]}"
      Mailer.deliver(email)  
      p = apt.purchase_for_by_friend(friend)
      if p.purchased?
        send_admin_email_helper("Cancelled appointment was purchased", 
          "#{friend.email} purchased appointment ##{apt.token}\nThis appointment was cancelled.", 
          "Inbox parser script - #{Time.now}")
        log("Sent admin an email about the problem")
      end
    end 
    apt.friends.size
  end

  # --------------------------------------------

  def find_appointment(data)
    apt = Appointment.find_by_token(data[:token])
  end

  def get_appointment(data)          
    apt = find_appointment(data)
    unless apt
      apt = Appointment.new
      apt.start_time = data[:start_time]
      apt.end_time = data[:end_time]
      apt.date = data[:date]   
      apt.time_zone = data[:time_zone] 
      apt.token = data[:token]
      apt.rescheduled = 0
      apt.cancelled = 0
      # apt.save
    end                                
    apt
  end         

  def find_user(data)
    user = User.find_by_email(data[:email]) 
  end
  
  def get_user(data, affiliate)                  
    user = find_user(data)
    if user
      log("  Found user #{user.first_name} #{user.last_name} in database. [#{affiliate.name}]")
      return user
    else
      user = User.new
      user.first_name = data[:first_name]
      user.last_name = data[:last_name]
      user.email = data[:email]

      # valid = user.save
      if user.valid?
        log("  Added user #{user.first_name} #{user.last_name} to database. [#{affiliate.name}]")
        return user
      else
        errors = user.errors.full_messages.inject('') { |s,e| s << "#{e}: " }
        log("[Error] Unable to create user from data: #{data.inspect}: Errors: #{errors}")
        return nil
      end
    end
  end

  def find_affiliate(data)
    unless data.key?(:business)    
      log("[Error] Unable to parse the business from this appointment: #{data.inspect}") 
      send_admin_email("Apt. notification email doesn't have Affiliate information", 
        "Email does not contain the Affiliate information.\nPlease update their Appointment Quest notification email.", 
        "Inbox parser script : #{Time.now}",
        data)
      log("Sent admin an email about the problem")
      return nil
    end
    affiliate = Affiliate.find_by_name(data[:business])
    unless affiliate
      log("[Error] Unable to find the affiliate '#{data[:business]}' for this appointment: #{data.inspect}")
      send_admin_email("Apt. notification email has invalid Affiliate name",
        "Unable to find a partner that matches this name: #{data[:business]}.\Please update the database with the correct name for this partner.", 
        "Inbox parser script : #{Time.now}",
        data)   
      log("Sent admin an email about the problem")
    end
    return affiliate
  end      
  
end         

# --------------------------------------------
# MAIN
# --------------------------------------------

def main(args)
    usage_and_quit if args.size == 0 or !%w(development test production).include? args[0].downcase

    now = Time.now
    $verbose = args.include? '-verbose'
    log("# Executing script on #{now}")
    
    # process the inbox and emails
    del = args.include? "-delete"
    af = AppointmentFind.new  
    env = args[0]
    af.process(env, del)
    log("Finished parser in #{Time.now-now} seconds")
end

def usage_and_quit
    puts <<-MSG    
Error: Unable to find the environment 
Usage: ruby appointment_find.rb environment [-delete] [-verbose]
  Where environment is 'development', 'test' or 'production'
  The option -delete will delete the emails from the inbox
    MSG
    exit(-1)
end

# if the script is being executed directly
if __FILE__ == $0         
  main(ARGV)
end

