require 'server_info'

class FriendController < ApplicationController

  include Usaepay

  before_filter :authorize, :except => [:denied, :approved, :login, :forgot_password, :confirm, :invite_friend, :canceled_appointment, :send_password] 
  before_filter :verify_friend, :except => [:denied, :approved, :login, :forgot_password, :confirm, :invite_friend, :canceled_appointment, :send_password] 
  before_filter :verify_appointment, :only => [:welcome, :view, :purchase, :thanks, :expired, :cant_purchase]

  layout "user"

  # --------------------------------------------

  def confirm            
    @friend = Friend.find_by_confirmation_code(params[:code])
    if @friend == nil                       
      display_error "Unable to find a user that matches this confirmation code: <br/>#{params[:code]}<p/>"
    else                
      @apt = Appointment.find_by_id(params[:apt_id])
      if @apt
        session[:friend_id] = @friend.id 
        unless @friend.answered_on 
          @friend.answered_on = Time.now
          @friend.save
        end
        redirect_to :action => "welcome", :id => @friend.id, :apt_id => @apt.id
      else
        display_error "Unable to find a matching appointment"
      end
    end
  end    
  
  # this handles the link in the invitation email, to "view the email if you can't see it"
  def invite_friend
    @friend = Friend.find_by_confirmation_code(params[:code])
    render_email_page('mailer/invite_friend')
  end            
  
  def canceled_appointment
    @friend = Friend.find_by_id(params[:id])
    render_email_page('mailer/canceled_appointment')
  end          
  
  def rescheduled_appointment
    @friend = Friend.find_by_id(params[:id])
    render_email_page('mailer/rescheduled_appointment')
  end

  # --------------------------------------------
  def welcome           
    # if purchased, render the approved page
    if @friend.purchase_for(@apt).purchased_on
      render :action => 'approved'
    # show the regular welcome page
    else
      set_purchase_variables
      render :action => 'welcome'
    end
  end
  
  def index
  end

  # --------------------------------------------

  # def edit_profile
  # end
  #      
  # def update_profile
  #   # there's a potential security risk.. of someone changing their email using form injection
  #   if @friend.update_attributes(params[:friend])
  #     flash[:notice] = "Your profile has been updated."
  #     redirect_to :action => "index", :id => @friend.id
  #   else
  #     render :action => :edit_profile, :id => @friend.id
  #   end
  # end

  def check   
    flash.now[:notice] = "You have already verified your brower compatability." if @friend.compatible == 1
    # if there is an apt_id parameter, verify the appointment.
    if params[:apt_id]
      verify_appointment
    end
  end
  
  # --------------------------------------------

  def view                  
    verify_appointment    

    # check for purchases and expired streams
    unless @friend.purchase_for(@apt).purchased_on
      if @apt.expired? and !@apt.has_purchases?    
        flash[:notice] = "This SonostreamLive has expired!"
        @days_ago = ((Time.now - @apt.start_time_t) / 1.day).to_i
        redirect_to :action => "expired", :id => @friend.id, :apt_id => @apt.id
        return
      else
        flash[:notice] = "You have not purchased this stream."
        redirect_to :action => "welcome", :id => @friend.id, :apt_id => @apt.id
        return
      end
    end 

    if @apt.early?
      render :action => "not_started", :id => @friend.id, :apt_id => @apt.id, :token => @apt.token
      return
    end

    if @apt.past?
      if @apt.archive_exists?
        save_viewing(View::ARCHIVED)
        render :action => "view_archive", :id => @friend.id, :apt_id => @apt.id, :token => @apt.token
        return
      else     
        # there was an error.  can't find an archive for a purchased stream
        subject =  "Charged a customer for stream that wasn't there"
        message =  "#{@friend.email} purchased the stream #{@apt.token} (#{@apt.user.email})\n\n"
        message += "The appointment was scheduled for #{@apt.full_time}.\n"
        message += "However, the stream was not found on the server."
        send_admin_email(subject, message, "Sonostream Application")
        render :action => "cant_find_archive", :id => @friend.id, :apt_id => @apt.id, :token => @apt.token
        return
      end
    end

    # today?
    if @apt.today?

      # make XML call to local wowza server, parse XML and turn the names of live streams
      stream = find_live_stream(@apt.token)

      if stream
        save_viewing(View::LIVE)
        @live = true
        render :action => "view_live", :id => @friend.id, :apt_id => @apt.id, :token => @apt.token
        return
      end

      # looks in the directory for the file and returns that
      if @apt.archive_exists?
        save_viewing(View::ARCHIVED)
        render :action => "view_archive", :id => @friend.id, :apt_id => @apt.id, :token => @apt.token
        return
      # they are early.  it's the day of the appointment but before it's live and before it's archived
      else
        render :action => "about_to_start", :id => @friend.id, :apt_id => @apt.id, :token => @apt.token
        return
      end
    end
  end

  def purchase             
    # is the user trying to purchase a stream that's not there for some reason?  don't allow them!
    if @apt.past? and @apt.archive_url == nil     
      flash[:notice] = "An error has been detected with this video stream"
      redirect_to :action => "cant_purchase", :id => @friend.id, :apt_id => @apt.id
    # is the user trying to purchase a stream that's not there for some reason?  don't allow them!
    elsif @apt.cancelled == 1     
      flash[:notice] = "This Sonostream appointment has been cancelled."
      redirect_to :action => "cant_purchase_cancelled", :id => @friend.id, :apt_id => @apt.id
    end
    set_purchase_variables
  end 
  
  def cant_purchase
  end    
            
  def expired
    @days_ago = ((Time.now - @apt.start_time_t) / 1.day).to_i
  end
                               
  # --------------------------------------------
  # approved and denied are callbacks set in the Usaepay system
  # they contain parameters about the purhcase, including an
  # invoice number -- Clearview_Ultrasound_3004_12
  # this invoice number identifies the affiliate, the apt token, and the friend
  # --------------------------------------------

  def approved                     
    # todo: add some validation to make sure people don't go straight here\
    # UMversion
    @apt, @friend = from_invoice_number(params, "friend")
    if @apt and @friend
      @friend.purchase(@apt)
      # do a login.. for some reason, login information is being lost when we transfer over from the billing stuff
      session[:friend_id] = @friend.id
    else
      # for some reason there's no parameter data in the post.. maybe someone typed in
      # approved to come straight here.  :/
      send_admin_notification("approved")
      redirect_to :action => "login"
    end
  end
  
  def denied 
    @apt, @friend = from_invoice_number(params, "friend")
    if params["UMerror"]
      @errors = params["UMerror"].split(',')
      set_purchase_variables
    else
      send_admin_notification("denied")
      redirect_to :action => "login"
    end
  end

  # --------------------------------------------

  def forgot_password
  end
  
  def send_password 
    if request.post?
      # todo refactor with the user/send_password
      email = params[:forgot][:email].downcase
      found = Friend.find_by_email(email)
      if found
        flash[:notice] = "Your password has been mailed to #{email}"
        # send the email
        email = Mailer.create_forgot_password(found, request, 'friend')  
        Mailer.deliver(email)
        redirect_to :action => "login"
      else
        flash[:notice] = "Unable to find a user with this email address: #{params[:forgot][:email]}"
        redirect_to :action => "forgot_password"
      end
    else
      redirect_to :action => "forgot_password"
    end
  end       
  
  # --------------------------------------------
  
  # todo refactor with user login
  def login
    # new attempt
    if request.get? 
      session[:friend_id] = nil
      @login = Friend.new
      # find the name to populate the email form
      friend = Friend.find_by_id(params[:id])
      @address = (friend.nil?) ? "" : friend.email
    else                                      
      unverified = Friend.new(params[:login])
      friend = unverified.login
      # valid attempt
      if friend
        session[:friend_id] = friend.id
        flash[:notice] = "#{friend.email} logged in" 
        # go to the url specified in the page parameter if it's present
        if params[:page] != nil
          redirect_to params[:page]
        else 
          # if there's only one invitation, go straight there
          if friend.appointments.count == 1
            redirect_to :action => "welcome", :id => friend.id, :apt_id => friend.appointments[0].id
          else
            redirect_to :action => 'index', :id => friend.id
          end
        end
      # invalid attempt
      else
        flash.now[:notice] = "Invalid user / password combination" 
        logger.info "[ALERT] Friend Login attempt failed.  Email=#{unverified.email} Password=#{unverified.password} IP=#{request.remote_ip}"
        render :action => 'login', :page => params[:page]   
      end
    end
  end     
  
  def logout
    flash[:notice] = "#{@friend.email} logged off."
    session[:friend_id] = nil
    redirect_to :action => "login"
  end
  
  # --------------------------------------------

  private 
  
  def set_purchase_variables
    @description = "SonostreamLive for #{@apt.user.full_name} on #{@apt.date}"
    @price = application_settings["purchase"]["price"]
    @invoice = usaepay_invoice_number(@apt, @friend)
    @form_url = usaepay_form_url(@price, @invoice, @description, "friend")
  end
  
  # every time a video is viewed.. we record it, and the ip which viewed it
  # this will let us detect abnormal behavior with streams
  def save_viewing(type)
    purchase = @apt.purchase_for_by_friend(@friend)
    if purchase.purchased?
      purchase.add_viewing(request.remote_ip, type)
    end
  end

  def find_live_stream(token)
    # talks to the wowza server and returns back an array of ServerInfo structs (name, status)
    streams = ServerInfo.server_infos
    # find a live stream that matches has a name that matches the token
    stream = streams.find {|stream| stream.name =~ Regexp.new(token)}
  end

  def render_email_page(email_path)
    if @friend == nil                       
      display_error "Unable to find the matching user."
    else                 
      @apt = Appointment.find_by_id(params[:apt_id])
      if @apt       
        @user = @apt.user
        @site_url = application_settings["site"]["url"]     
        @skip_site_link = true
        render :template => email_path
      else
        display_error "Unable to find a matching appointment"
      end
    end
  end 
                        
  def authorize
    unless session[:friend_id]
      flash[:notice] = "Please login before accessing that page"
      redirect_to :controller => 'friend', :action  => 'login', :page => request.request_uri     
    end
  end      
  
  def verify_friend
    @friend = Friend.find_by_id(params[:id])
    display_error "Unable to find friend #{params[:id]}" unless @friend
  end

  def verify_appointment
    @apt = Appointment.find_by_id(params[:apt_id])
    display_error "Unable to find appointment #{params[:apt_id]}" unless @apt
  end

end
