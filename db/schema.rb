# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130126193450) do

  create_table "affiliates", :force => true do |t|
    t.string  "name",     :limit => 50
    t.integer "active",                 :default => 1
    t.integer "is_admin",               :default => 0
    t.string  "email",    :limit => 50
    t.string  "password", :limit => 50
  end

  create_table "appointments", :force => true do |t|
    t.integer  "user_id",                                        :null => false
    t.string   "start_time",        :limit => 50
    t.string   "end_time",          :limit => 50
    t.string   "date",              :limit => 50
    t.string   "time_zone",         :limit => 10
    t.string   "token",             :limit => 50
    t.string   "confirmation_code", :limit => 50
    t.integer  "rescheduled",                     :default => 0
    t.integer  "cancelled",                       :default => 0
    t.integer  "video_removed",                   :default => 0
    t.datetime "created_on"
    t.datetime "confirmed_on"
  end

  create_table "friends", :force => true do |t|
    t.string   "email",             :limit => 100
    t.string   "password",          :limit => 50,  :default => ""
    t.string   "confirmation_code", :limit => 50
    t.datetime "created_on"
    t.datetime "answered_on"
    t.integer  "compatible",                       :default => 0
  end

  create_table "purchases", :force => true do |t|
    t.integer  "appointment_id"
    t.integer  "friend_id"
    t.datetime "purchased_on"
    t.decimal  "price",          :precision => 6, :scale => 2
    t.integer  "user_id"
    t.boolean  "by_affiliate",                                 :default => false
  end

  add_index "purchases", ["appointment_id", "friend_id"], :name => "appointment_id"

  create_table "sessions", :force => true do |t|
    t.string   "session_id"
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], :name => "sessions_session_id_index"

  create_table "users", :force => true do |t|
    t.integer  "affiliate_id",                                       :null => false
    t.string   "first_name",   :limit => 50
    t.string   "last_name",    :limit => 50
    t.string   "email",        :limit => 50
    t.string   "password",     :limit => 50, :default => "password"
    t.datetime "created_on"
  end

  create_table "views", :force => true do |t|
    t.integer  "purchase_id"
    t.datetime "viewed_on"
    t.string   "view_type",   :limit => 10
    t.string   "ip_address",  :limit => 20
  end

end
