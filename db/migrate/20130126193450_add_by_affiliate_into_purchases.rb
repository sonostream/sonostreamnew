class AddByAffiliateIntoPurchases < ActiveRecord::Migration
  def self.up
    add_column :purchases, :affiliate_id, :integer, :default => 0
  end

  def self.down
    remove_column :purchases, :affiliate_id
  end
end
