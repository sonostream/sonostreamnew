# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_sonostream_session',
  :secret      => '4876d3344562eee06708303dcdbe5752bb891306a3fe69aa6116c30d27686059aeb0e056842ca265cb9af50c5df4ba5fa507644df7f609e03bb31c1b54957fee'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
