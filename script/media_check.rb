#!/usr/bin/env ruby     
require File.dirname(__FILE__)+'/../config/environment'
require 'parsedate'

class String
  def to_time
    res = ParseDate.parsedate(self)
    Time.local(*res)
  end
end

class Time
  def our_format
    strftime('%m/%d/%Y')
  end
end

date = ARGV[0] || Time.now.our_format
days = ARGV[1] || "7"

usage = <<-END
Usage: ruby find_appointment.rb <starting-date> <num-days>
  <starting-date> : The date to start looking.    i.e. 04/13/2007
  <num-days>      : The number of days to check.  i.e. 7
END

unless date =~ /\d\d\/\d\d\/\d\d\d\d/
  puts "Error: Not a valid date: #{date}"
  puts usage
  exit(-1)
end

unless days =~ /\d+/ and days.to_i > 0
  puts "Error: Not a valid date range: #{days}"
  puts usage
  exit(-1)
end

def find_appointments_for_date(date)
  appointments = Appointment.find(:all, :conditions=>['date = ? and purchases.purchased_on is not NULL', date], 
    :include => :purchases)
  appointments.map do |a|
    media_present = a.archive_url ? "Yes" : "No"
    "#{a.token},#{a.full_time},#{media_present}, #{a.user.affiliate.name}"
  end
end

days = days.to_i
date_t = date.to_time
(0..days).each do |number|
  the_date = (date_t + number*86400).our_format
  puts find_appointments_for_date(the_date)
end
