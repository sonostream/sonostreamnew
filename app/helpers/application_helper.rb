# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper

  def display_information_box 
    error = flash[:error] || flash[:notice]
    if error
      content = <<-EOS
  		  <div class="roundedWrapper">
  		    <div class="rounded">
  		        <img class="borderTL" src="/images/borderTL.gif" alt="&nbsp;" width="12" height="12" />
  		        <img class="borderTR" src="/images/borderTR.gif" alt="&nbsp;" width="12" height="12" />
  				<table>
  				  <tr>
  				    <td valign="middle"><img src="/images/warning_icon.gif" /></td>
  				    <td><span class="style2">#{error}</span></td>
  				 </tr>
  				</table>
  			<div class="roundedCornerSpacer">&nbsp;</div> <!-- Fix for IE5/Win -->
  			</div><!-- end of div.content -->
  		    <div class="bottomCorners">
  		        <img class="borderBL" src="/images/borderBL.gif" alt="&nbsp;" width="12" height="12" />
  		        <img class="borderBR" src="/images/borderBR.gif" alt="&nbsp;" width="12" height="12" />
  		    </div>
  		</div><!-- end of div.contentWrapper --> 
  		<p />
      EOS
      return content
    end
  end
  
  def display_apt_date(apt)
    "#{apt.date} from #{apt.start_time} to #{apt.end_time} (#{apt.time_zone})"
  end

end
