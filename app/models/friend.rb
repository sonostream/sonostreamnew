require 'constants'
require 'digest/sha1'

class Friend < ActiveRecord::Base

  include Constants
  
  has_many :purchases
  has_many :appointments, :through => :purchases

  # --------------------------------------------
  
  validates_presence_of :email    
  validates_uniqueness_of :email
  validates_format_of :email, :with  => EMAIL_REGEX, :message  => "is not a valid email format"

  # --------------------------------------------

  # overrides 'login' from Constants
  def login
     Friend.find(:first, :conditions  => ["email = ?", email.downcase])
  end


  def purchase_for(appointment)
    Purchase.find_by_appointment_id_and_friend_id(appointment.id, id)
  end

  def link(appointment)  
    unless purchase_for(appointment)
      purchase = Purchase.new do |p|
        p.friend_id = id.to_s  # the associations don't work if id = 2 .. it must be id = "2"
        p.appointment_id = appointment.id.to_s   
        p.save
      end
    end
  end
  
  def purchase(appointment)
    link(appointment)
    purchase = purchase_for(appointment)
    purchase.purchased_on = Time.now 
    purchase.price = application_settings["purchase"]["price"].to_f
    purchase.save
  end
  
  # --------------------------------------------

  def before_save
    generate_confirmation_code 
  end

  def generate_confirmation_code
    unless self.confirmation_code
      key = ( self.email or rand(1000000).to_s ) + "--sonostream---"
      self.confirmation_code = Digest::SHA1.hexdigest(key) 
    end
  end                     

  def full_name
    "#{first_name} #{last_name}"
  end
  
end
