require 'parsedate'

class Appointment < ActiveRecord::Base

  belongs_to :user                                               
  has_many :purchases
  has_many :friends, :through => :purchases
  has_many :views, :through => :purchases
  
  validates_presence_of :token
  # --------------------------------------------
  
  def before_save
    generate_confirmation_code
  end
  
  # new appointments will have a unqiue code associated with them
  def generate_confirmation_code
    unless self.confirmation_code
      id = "#{full_time}-#{Time.now}" 
      self.confirmation_code = Digest::SHA1.hexdigest(id) 
    end
  end  

  def purchase_for_by_friend(friend)
    Purchase.find_by_appointment_id_and_friend_id(id, friend.id)
  end
  
  def purchase_for_by_user
    Purchase.find_by_appointment_id_and_user_id(id, user.id)
  end
  
  def purchase_for_by_affiliate_id(affiliate_id)
    Purchase.find_by_appointment_id_and_affiliate_id(id, affiliate_id)
  end
  
  def confirmed 
    confirmed_on = Time.now
    save
  end
  
  # helper method for sending admin emails with error messages
  def send_admin_email(subject, message, sent_from, data=nil) 
    msg = "#{message}"
    msg += "\n\nData:    #{data.inspect}" if data
    e = Mailer.create_admin_email(subject, msg, sent_from)              
    Mailer.deliver(e)    
  end
  
  # --------------------------------------------

  def full_time
    "#{Date.parse(date).strftime('%m/%d/%Y')}, #{start_time} - #{end_time} #{time_zone}"
  end     

  def start_time_t  
    to_time_object(:start_time)
  end

  def end_time_t
    to_time_object(:end_time)
  end 

  # --------------------------------------------

  def expired?
    num_days = application_settings["site"]["days_to_keep_media"]
    if num_days    
      return start_time_t + num_days.days < Time.now
    else      
      subject =  "Missing entry in config/settings.yml"
      message =  "Expected a number in config/settings.yml\n"
      message += "site:\n"
      message += "  days_to_keep_media: 7"
      send_admin_email(subject, message, "Sonostream Application")
      return false
    end
  end      
  
  # has anyone purchased the stream for this appointment?
  def has_purchases?
    self.purchases.detect { |p| p.purchased_on != nil }
  end
  
  # --------------------------------------------

  def archive_url(type = 'web')
    if archive_exists?
      # for 'web', look at config/settings.yml for video/archive_url/web
      url_template = application_settings['video']['archive_url'][type.to_s]
      # substitute the %s in the URL with the token 
      url_template % token
    end
  end
 
  def live_url(type)
    # for 'web', look at config/settings.yml for video/live_url/web
    url_template = application_settings['video']['live_url'][type.to_s]
    # substitute the %s in the URL with the token 
    url_template % token
  end          

  def archive_filename
    "sonostream_#{token}.mp4"
  end

  def archive_full_path
    dir = application_settings['video']['directory']
    path = File.expand_path(archive_filename, dir)    
  end

  def archive_exists?
    File.exists?(archive_full_path)
  end


  # --------------------------------------------
  
  def today?
    start_time_t.to_date == Time.now.to_date
  end
  
  def past?
    Time.now.to_date > start_time_t.to_date
  end
  
  def early?
    Time.now.to_date < start_time_t.to_date
  end
  
  def duration
    ((end_time_t - start_time_t)/60.0).to_i
  end

  # --------------------------------------------
  
  def to_time_object(time_field)
    zone = time_zone || Time.now.zone     
    time = send(time_field)
    r = ParseDate.parsedate("#{date} #{time} #{zone}")
    return Time.local(*r) + time_zone_delta.hours
  end 
  
  def time_zone_delta  
    server_time_zone_delta = (Time.now.utc_offset / (60*60)).to_i
    delta = case time_zone
      when /EDT/:     -4
      when /Eastern/: -5
      when /EST/:     -5
      when /CDT/:     -5
      when /Central/: -6  
      when /CST/:     -6
      when /MDT/:     -6
      when /Mountain/: -7
      when /MST/:     -7
      when /PDT/ :    -7
      when /Pacific/: -8
      when /PST/:     -8
      else
        logger.error("Found an unknown time zone (#{time_zone}) for "+self.to_s)
        0
    end
    server_time_zone_delta - delta
  end
  
end
