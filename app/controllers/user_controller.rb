class UserController < ApplicationController

  include Usaepay

  before_filter :authorize, :except => [:denied, :approved, :login, :forgot_password, :confirm, :invite_mom, :send_password, :purchase_mom] 
  before_filter :verify_user, :except => [:download, :denied, :approved, :login, :forgot_password, :confirm, :invite_mom, :send_password, :purchase_mom] 
  before_filter :verify_appointment, :only => [:download, :welcome, :view, :purchase, :invite, :update_invite, :expired, :cant_purchase]
  
  # this link is sent as an invitation to the mother
  def confirm
    code = params[:code]
    @apt = Appointment.find_by_confirmation_code(code)
    if @apt == nil                       
      display_error "Unable to find a user that matches this confirmation code: <br/>#{code}<p/>"
    else
      if @apt.user
        session[:user_id] = @apt.user.id
        @apt.confirmed
        redirect_to :action => "welcome", :id => @apt.user.id, :apt_id => @apt
      else
        display_error "Unable to find a user that matches this appointment: <br/>#{@apt.full_time}<p/>"
      end
    end
  end         
  
  # lets the mom view the email on the site
  def invite_mom
    code = params[:code]
    @apt = Appointment.find_by_confirmation_code(code)
    if @apt == nil                       
      display_error "Unable to find a user that matches this confirmation code: <br/>#{code}<p/>"
    else
      @user = @apt.user
      @site_url = application_settings["site"]["url"]     
      @skip_site_link = true
      render :template => 'mailer/invite_mom'
    end
  end

  # lets the mom view the email on the site
  def purchase_mom
    apt_id = params[:apt_id]
    @apt = Appointment.find_by_id(apt_id)
    if @apt == nil                       
      display_error "Unable to find a appointment that matches this apt_id: <br/>#{apt_id}<p/>"
    else
      @user = @apt.user
      @site_url = application_settings["site"]["url"]     
      @skip_site_link = true
      render :template => 'mailer/purchase_mom'
    end
  end

  def purchase
    @apt = Appointment.find_by_id(params[:apt_id])
    
    # is the user trying to purchase a stream that's not there for some reason?  don't allow them!
    if @apt.past? and @apt.archive_url == nil     
      flash[:notice] = "An error has been detected with this video stream"
      redirect_to :action => "cant_purchase", :id => @user.id, :apt_id => @apt.id
    # is the user trying to purchase a stream that's not there for some reason?  don't allow them!
    elsif @apt.cancelled == 1     
      flash[:notice] = "This Sonostream appointment has been cancelled."
      redirect_to :action => "cant_purchase_cancelled", :id => @friend.id, :apt_id => @apt.id
    end
    set_purchase_variables
  end
  
  def cant_purchase
  end   
  
  def expired
    @days_ago = ((Time.now - @apt.start_time_t) / 1.day).to_i
  end
  # --------------------------------------------

  # /user/welcome/1/1
  def welcome
    # if purchased, render the approved page
    if @user.purchase_for(@apt).try(:purchased_on)
      render :action => 'approved'
    # show the regular welcome page
    else
      set_purchase_variables
      render :action => 'welcome'
    end
  end

  # /user/invite/1/1
  def invite                    
    if @apt.expired? and !@apt.has_purchases?    
      @days_ago = ((Time.now - @apt.start_time_t) / 1.day).to_i
      render :action => 'expired', :id => @user.id, :apt_id => @apt.id
    else
      @friend = Friend.new   
      render :action => 'invite', :id => @user.id, :apt_id => @apt.id
    end
  end         
  
  def update_invite   
    friend = Friend.new(params[:friend])  
    
    # opps.. they entered the same email in.. we will resend the invitation
    found = @apt.friends.detect {|f| f.email.eql? friend.email.downcase}
    if found
      handle_invite(found, 'resent')
    else   
      # already in the database
      exists = Friend.find_by_email(friend.email)
      if exists
        exists.link(@apt)  # update the db relationship
        handle_invite(exists, 'sent')
      else
        if friend.save
          friend.link(@apt)  # update the db relationship
          handle_invite(friend, 'sent')
        else
          @friend = friend # this allows the error messages to display on validation
          render :action => 'invite'
        end  
      end
    end

  end

  def handle_invite(friend, action)
    @friend = friend
    send_invite_friend_email(@friend)
    flash[:notice] = "Invitation has been #{action} to #{friend.email}"
    redirect_to :action => 'welcome', :id => @user.id, :apt_id => @apt.id
  end

  # --------------------------------------------

  # /user/index/1 (no appointment selected)
  def index
    #if @user.appointments.count == 1
    #  redirect_to :controller => 'user', :action => 'welcome', :id => @user.id, :apt_id => @user.appointments[0]
    #end
  end

  def profile
  end  
  
  def edit_profile
  end

  def update_profile
    if @user.update_attributes(params[:user])
      flash[:notice] = "Your profile has been updated."
      redirect_to :action => "index", :id => @user.id
    else
      render :action => :edit_profile, :id => @user.id 
    end
  end
  
  def view                  

    # check for purchases and expired streams
    unless @user.purchase_for(@apt).try(:purchased_on)
      if @apt.expired? and !@apt.has_purchases?    
        flash[:notice] = "This SonostreamLive has expired!"
        @days_ago = ((Time.now - @apt.start_time_t) / 1.day).to_i
        redirect_to :action => "expired", :id => @user.id, :apt_id => @apt.id
        return
      else
        flash[:notice] = "You have not purchased this stream."
        redirect_to :action => "welcome", :id => @user.id, :apt_id => @apt.id
        return
      end
    end 

    if @apt.early?
      render :action => "not_started", :id => @user.id, :apt_id => @apt.id, :token => @apt.token
      return
    end

    if @apt.past?
      if @apt.archive_exists?
        save_viewing(View::ARCHIVED)
        render :action => "view_archive", :id => @user.id, :apt_id => @apt.id, :token => @apt.token
        return
      else     
        # there was an error.  can't find an archive for a purchased stream
        subject =  "Charged a customer for stream that wasn't there"
        message =  "#{@user.email} purchased the stream #{@apt.token} (#{@apt.user.email})\n\n"
        message += "The appointment was scheduled for #{@apt.full_time}.\n"
        message += "However, the stream was not found on the server."
        send_admin_email(subject, message, "Sonostream Application")
        render :action => "cant_find_archive", :id => @user.id, :apt_id => @apt.id, :token => @apt.token
        return
      end
    end

    # today?
    if @apt.today?

      # make XML call to local wowza server, parse XML and turn the names of live streams
      stream = find_live_stream(@apt.token)

      if stream
        save_viewing(View::LIVE)
        @live = true
        render :action => "view_live", :id => @user.id, :apt_id => @apt.id, :token => @apt.token
        return
      end

      # looks in the directory for the file and returns that
      if @apt.archive_exists?
        save_viewing(View::ARCHIVED)
        render :action => "view_archive", :id => @user.id, :apt_id => @apt.id, :token => @apt.token
        return
      # they are early.  it's the day of the appointment but before it's live and before it's archived
      else
        render :action => "about_to_start", :id => @user.id, :apt_id => @apt.id, :token => @apt.token
        return
      end
    end
  end
  
  # --------------------------------------------
  # approved and denied are callbacks set in the Usaepay system
  # they contain parameters about the purhcase, including an
  # invoice number -- Clearview_Ultrasound_3004_12
  # this invoice number identifies the affiliate, the apt token, and the user
  # --------------------------------------------

  def approved                     
    # todo: add some validation to make sure people don't go straight here\
    # UMversion
    @apt, @user = from_invoice_number(params, "user")
    if @apt and @user
      @user.purchase(@apt)
      # do a login.. for some reason, login information is being lost when we transfer over from the billing stuff
      session[:friend_id] = @user.id
    else
      # for some reason there's no parameter data in the post.. maybe someone typed in
      # approved to come straight here.  :/
      send_admin_notification("approved")
      redirect_to :action => "login"
    end
  end
  
  def denied 
    @apt, @user = from_invoice_number(params, "user")
    if params["UMerror"]
      @errors = params["UMerror"].split(',')
      set_purchase_variables
    else
      send_admin_notification("denied")
      redirect_to :action => "login"
    end
  end
      
  # --------------------------------------------

  def forgot_password
  end
  
  def send_password
    if request.post?
      email = params[:forgot][:email].downcase
      found_user = User.find_by_email(email)
      if found_user
        flash[:notice] = "Your password has been mailed to #{email}"
        # send the email
        email = Mailer.create_forgot_password(found_user, request, 'user')  
        Mailer.deliver(email)
        redirect_to :action => "login"
      else
        flash[:notice] = "Unable to find a user with this email address: #{params[:forgot][:email]}"
        redirect_to :action => "forgot_password"
      end           
    else
      redirect_to :action => 'forgot_password'
    end
  end
  
  # --------------------------------------------
  
  def login
    # new attempt
    if request.get? 
      session[:user_id] = nil
      @login = User.new
      # do not populate the email form.. it's a security hole..
      # it let's you grab emails by changing the id by hand in the URL
      @address = "" 
    else                                      
      unverified_user = User.new(params[:user])
      user = unverified_user.login
      # valid attempt
      if user
        session[:user_id] = user.id
        flash[:notice] = "#{user.full_name} logged in"
        # if the user only has one appointment, take him directly to that appointment
        redirect_to :controller => 'user', :id => user.id, :action => 'index'
      # invalid attempt
      else
        flash.now[:notice] = "Invalid user / password combination" 
        logger.info "[ALERT] User Login attempt failed.  Email=#{unverified_user.email} Password=#{unverified_user.password} IP=#{request.remote_ip}"
        render :action => 'login', :page => params[:page]   
      end
    end
  end     
  
  def logout
    flash[:notice] = "#{@user.full_name} logged off."
    session[:user_id] = nil
    redirect_to :action => "login", :id => params[:id]
  end
  
  # -------------------------------------------
  def download
    send_file "#{application_settings['video']['directory']}sonostream_#{@apt.token}.mp4" , :disposition => 'inline'
  end
  
  # --------------------------------------------

  private 

  def set_purchase_variables
    @description = "SonostreamLive for #{@apt.user.full_name} on #{@apt.date}"
    @price = application_settings["purchase"]["price"]
    @invoice = usaepay_invoice_number(@apt, @user)
    @form_url = usaepay_form_url(@price, @invoice, @description, "user")
  end
                        
  def send_invite_friend_email(friend)  
    # send the email             
    email = Mailer.create_invite_friend(@user, friend, @apt)  
    email.set_content_type("text/html")
    Mailer.deliver(email)
  end
  
  # every time a video is viewed.. we record it, and the ip which viewed it
  # this will let us detect abnormal behavior with streams
  def save_viewing(type)
    purchase = @apt.purchase_for_by_user
    if purchase.purchased?
      purchase.add_viewing(request.remote_ip, type)
    end
  end

  def find_live_stream(token)
    # talks to the wowza server and returns back an array of ServerInfo structs (name, status)
    streams = ServerInfo.server_infos
    # find a live stream that matches has a name that matches the token
    stream = streams.find {|stream| stream.name =~ Regexp.new(token)}
  end
  
  def authorize
    unless session[:user_id]
      flash[:notice] = "Please login before accessing that page"
      redirect_to :controller => 'user', :action  => 'login', :id  => params[:id], :page => request.request_uri     
    end
  end      
  
  def verify_user
    @user = User.find_by_id(params[:id])
    display_error "Unable to find user #{params[:id]}" unless @user
  end

  def verify_appointment
    @apt = Appointment.find_by_id(params[:apt_id])
    display_error "Unable to find appointment #{params[:apt_id]}" unless @apt
  end
  
end
