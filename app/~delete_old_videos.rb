require File.dirname(__FILE__) + '/../config/environment'

def log(msg)
   $parser_log ||= Logger.new(File.dirname(__FILE__) + '/../log/video_deleter.log')
   $parser_log.info(msg)
   puts msg if $verbose
end

def video_dir
	dir = application_settings['video']['directory']
end                             

def video_files
	filenames = Dir.glob(video_dir+'*.asf')
	files = filenames.collect {|filename| File.new(filename)}
end        

def old?(file, time)
	file.mtime + time < Time.now
end

# get the Appointment from the database that matches the token in the filename
def appointment(file)
	filename = File.basename(file.path)
	# pull the token from the filename: Sonostream_30005 and Sonostream_30005_001
	if filename =~ /\w+_(\d+)_*/  
    return Appointment.find_by_token($1)
  end
end

def usage_and_quit
    puts <<-MSG    
Usage: ruby delete_old_videos.rb [-verbose] [-override]
  Deletes old media files on the server.  See config/setings.yml
  to configure how long the files are left on the server.
  Files that were purchased by friends are not deleted, 
  unless the -override flag is given.
    MSG
    exit(-1)
end

def main(args)

  number_of = application_settings["site"]["days_to_keep_media"]

  $verbose = args.include? '-verbose'
  $override = args.include? '-override'          
  if args.include? '-h' or args.include? '--help'
    usage_and_quit
  end
                                     
  log(" ")
  log("# Executing script on #{Time.now}")
    
  files = video_files 
  log("Found #{files.size} files in #{video_dir}")
  old_files = files.find_all {|f| old?(f, number_of.days)}
  log("Found #{old_files.size} files more than #{number_of} days old") 
  # determine the files to delete
  if $override
    deleting_files = old_files
  else
    deleting_files = old_files.find_all do |f|
      apt = appointment(f) # try to find an appointment for this file
      if apt
        unless should_keep?(apt)
          apt.video_removed = 1
          apt.save
          true
        else
          log(" Not deleting #{f.path}. Purchased.")
          false
        end
      else  
        # delete files when there's no corresponding appointment in the db.
        true
      end
    end
    log("Found #{deleting_files.size} old files for deleting")
  end    
  # delete the files
  deleting_files.each do |f|
    log("  Deleting #{f.path}")
    f.close # was causing problems on windows. "can't delete.  file in use"
    File.delete(f.path)
  end
end

# do not delete files for appointments that have been purchased
def should_keep?(apt) 
  apt.purchases.detect { |p| p.purchased_on != nil}
end

                                                        
# if the script is being executed directly
if __FILE__ == $0         
  require File.dirname(__FILE__) + '/../config/environment'
  main(ARGV)
end

                                 
