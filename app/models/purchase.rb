
class Purchase < ActiveRecord::Base

  belongs_to :friend
  belongs_to :user
  belongs_to :affiliate
  belongs_to :appointment 
  has_many :views
  
  def purchased?
    purchased_on != nil
  end           
  
  # 2007-05-13
  def purchase_date
    purchased_on.strftime("%Y-%m-%d") if purchased_on
  end
  
  def add_viewing(from_ip_address, type, time = Time.now)
    view = View.new
    view.purchase_id = self.id
    view.viewed_on = time                
    view.view_type = type.to_s
    view.ip_address = from_ip_address
    view.save
  end

  def Purchase.all_purchased
    Purchase.find(:all, :conditions => ["purchased_on is not NULL"])
  end

end