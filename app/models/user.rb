require 'constants'

class User < ActiveRecord::Base

  include Constants
  
  belongs_to :affiliate   
  has_many :appointments
  has_many :purchases
  
  validates_presence_of :email
  validates_uniqueness_of :email
  validates_format_of :email, :with  => EMAIL_REGEX, :message  => "is not a valid email format", :if => :email?
  
  validates_presence_of :first_name 
  validates_format_of :first_name, :with  => NAME_REGEX, :message  => INVALID_CHARACTER_MSG, :if => :first_name?
  validates_presence_of :last_name 
  validates_format_of :last_name, :with  => NAME_REGEX, :message  => INVALID_CHARACTER_MSG, :if => :last_name?
                        
  def full_name
    "#{first_name} #{last_name}"
  end               
  
  def login
     User.find(:first, :conditions  => ["email = ? and password = ?", email, password])
  end  
  
  def purchase_for(appointment)
    Purchase.find_by_appointment_id_and_user_id(appointment.id, id)
  end

  def link(appointment)  
    unless purchase_for(appointment)
      purchase = Purchase.new do |p|
        p.user_id = id.to_s  # the associations don't work if id = 2 .. it must be id = "2"
        p.appointment_id = appointment.id.to_s   
        p.save
      end
    end
  end
  
  def purchase(appointment, affiliate_id = 0)
    link(appointment)
    purchase = purchase_for(appointment)
    purchase.purchased_on = Time.now 
    purchase.affiliate_id = affiliate_id
    purchase.price = application_settings["purchase"]["price"].to_f
    purchase.save
  end
end
