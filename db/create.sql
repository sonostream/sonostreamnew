DROP TABLE IF EXISTS `affiliates`;
CREATE TABLE `affiliates` (
  `id` int(20) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `active` int(2) default 1,
  `is_admin` int(2) default 0,
  `email` varchar(50) default NULL,
  `password` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(20) NOT NULL auto_increment,
  `affiliate_id` int(20) NOT NULL,
  `first_name` varchar(50) default NULL,
  `last_name` varchar(50) default NULL,
  `email` varchar(50) default NULL,
  `password` varchar(50) default 'password',
  `created_on` datetime default NULL, 
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `appointments`;
CREATE TABLE `appointments` (
  `id` int(20) NOT NULL auto_increment,
  `user_id` int(20) NOT NULL,
  `start_time` varchar(50) default NULL, 
  `end_time` varchar(50) default NULL, 
  `date` varchar(50) default NULL, 
  `time_zone` varchar(10) default NULL, 
  `token` varchar(50) default NULL, 
  `confirmation_code` varchar(50) default NULL, 
  `rescheduled` int(2) default 0,
  `cancelled` int(2) default 0,
  `video_removed` int(2) default 0,
  `created_on` datetime default NULL, 
  `confirmed_on` datetime default NULL, 
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;


DROP TABLE IF EXISTS `friends`;
CREATE TABLE `friends` (
  `id` int(20) NOT NULL auto_increment,
  `email` varchar(100) default NULL,
  `password` varchar(50) default '',
  `confirmation_code` varchar(50) default NULL,
  `created_on` datetime default NULL, 
  `answered_on` datetime default NULL, 
  `compatible` int(2) default 0,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `purchases`;
CREATE TABLE `purchases` (
  `id` int(20) NOT NULL auto_increment,
  `appointment_id` int(20),
  `friend_id` int(20),   
  `purchased_on` datetime default NULL, 
  `price` decimal(6,2) default NULL,
  PRIMARY KEY  (`id`),
  INDEX  (`appointment_id`, `friend_id`)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `views`;
CREATE TABLE `views` (
  `id` int(20) NOT NULL auto_increment,
  `purchase_id` int(20),
  `viewed_on` datetime default NULL, 
  `view_type` varchar(10) default NULL,
  `ip_address` varchar(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB;    

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `id` int(20) NOT NULL auto_increment,
  `session_id` varchar(255) default NULL,
  `data` text,
  `created_at` datetime default NULL,
  `updated_at` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `sessions_session_id_index` (`session_id`)
) ENGINE=InnoDB;




