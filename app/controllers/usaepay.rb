require 'digest/md5' 

module Usaepay  
  
  def usaepay_form_url(amount, invoice, description=nil, user_type = "user", type = "sale")
    config = application_settings
    source = config["billing_integration_#{user_type}"]["source"]
    key = config["billing_integration_#{user_type}"]["key"]
    pin = config["billing_integration_#{user_type}"]["pin"]
    url = config["billing_integration_#{user_type}"]["url"]  
    url += "/" unless url.last == "/"   
    md5_hash = md5("#{type}:#{pin}:#{amount}:#{invoice}:") # the extra colon on the end is important
    form_url = "#{url}#{key}/#{type}?UMamount=#{amount.to_s}&UMinvoice=#{invoice}&UMmd5hash=#{md5_hash}"
    form_url += "&UMdescription=#{CGI.escape(description)}" if description
    form_url
  end              

  # we use an invoice number that lets us track the media, the affiliate and the purchaser
  def usaepay_invoice_number(apt, friend_or_user)
    # Clearview_Ultrasound_3004_12 <-- affiliate_token_friendId  
    affiliate = apt.user.affiliate.name
    "#{safe_name(affiliate)}_#{apt.token}_#{friend_or_user.id}"
  end                      
  
  def from_invoice_number(params, type) 
    # Clearview_Ultrasound_3004_12  -> token: => 3004, friend_id: => 12
    invoice_number = params[:UMinvoice]
    if invoice_number =~ /_(\d+)_(\d+)$/
      apt = Appointment.find_by_token($1)
      friend = type == "user" ? User.find_by_id($2): Friend.find_by_id($2)
      return apt,friend
    end
  end
  
  private
  
  def md5(source)
     Digest::MD5.hexdigest(source.to_s)
  end

  def safe_name(string)
    string.gsub(/[^A-Za-z0-9_]/,"_") 
  end
  
end