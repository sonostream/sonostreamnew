@echo off
echo Installing the Old Filter Deleter script
schtasks /create /tn "Sonostream Old File Deleter" /sc daily /mo 1 /st 02:00:00 /tr "c:\ruby\bin\ruby.exe c:\sonostream\app\delete_old_videos.rb -verbose"
echo Installing the Inbox parser script
schtasks /create /tn "Sonostream Inbox Parser" /sc minute /mo 10 /tr "c:\ruby\bin\ruby.exe c:\sonostream\app\appointment_find.rb production -verbose -delete"
schtasks /query