require 'rubygems'  
require 'active_record'

require 'net/pop'
require 'yaml'
require 'logger'         
require 'date'

require File.dirname(__FILE__) + '/../config/environment'

# --------------------------------------------

def log(msg)
   $parser_log ||= Logger.new(File.dirname(__FILE__) + '/../log/mommy_invite.log')
   $parser_log.info(msg)
   puts msg if $verbose
end

class MommyInvite

  def process(environment, delete_from_mailbox=true, save_email=false)
    now = Time.now
    # initialize database
    conf = YAML::load(File.open(File.dirname(__FILE__) + '/../config/database.yml'))
    ActiveRecord::Base.establish_connection(conf[environment])

    
    dir = File.expand_path('*', application_settings['video']['directory'])
    log("Checking #{dir} for video files") if $verbose
    files = Dir.glob(dir)
    today = Date.today
    
    # Search archive directory for archives created for today
    today_files = files.select do |filename|
      File.mtime(filename).to_date == today
    end
    log("Found the following archives for today: #{today_files.join(',')}")
    today_tokens = get_tokens(today_files)
    check_appointments(today_tokens, 1)
    
    # Search archive directory for archives created for last application_settings['file_expired']['days'] days
    recent_files = files.select do |filename|
      File.mtime(filename).to_date > today - 7 and File.mtime(filename).to_date != today
    end
    log("Found the following archives for last 7 days: #{recent_files.join(',')}")
    recent_tokens = get_tokens(recent_files)
    check_appointments(recent_tokens, 2)
    
    # Search archive directory for archives created before application_settings['file_expired']['days'] day
    seven_files = files.select do |filename|
      File.mtime(filename).to_date == today - application_settings['file_expired']['days'].to_i
    end
    log("Found the following archives before 7 day: #{seven_files.join(',')}")
    seven_tokens = get_tokens(seven_files)
    check_appointments(seven_tokens, 3)
    
    # Remove not purchased appointments in 8 days
    expired_files = files.each do |filename|
      if File.mtime(filename).to_date < today - application_settings['file_expired']['days'].to_i
        token = File.basename(filename).scan(/\d+/).first
        appointment = Appointment.find_by_token token
        
        if appointment and appointment.user.purchase_for(appointment).nil? and appointment.friends.length == 0
          # remove file and record.
          appointment.delete
          File.delete(filename)
        end 
      end
    end
    
  end
  
  def get_tokens(files)
    # take /usr/local/WowzaMediaServer/content/sonostream_300167398951.mp4 => 300167398951
    # remove any duplicates because 300167398951 and 300167398951_1 will show up twice
    tokens = files.map do |f|
      File.basename(f).scan(/\d+/).first
    end.flatten.uniq 
    
    return tokens
  end
  
  def check_appointments(tokens, template_index)
    log("Checking the following tokens: #{tokens.join(',')} ")
    
    tokens.each do |token|
      appointment = Appointment.find_by_token token
            
      if appointment
        user = appointment.user
        purchase = 
        if user.purchase_for(appointment).nil?
          email = user.email rescue nil
          if email 
            log("  Sending mommy purchase email to #{email} for #{token}")
            send_mommy_purchase(appointment, template_index)
          else
            log("  Could not find mommy email for appointment #{appointment.inspect}.  User #{user.inspect}")
          end
        else
          log("  #{user.full_name} is already purchased this appointment #{token}")
        end
      else
        log("  Could not find appointment with token #{token}")
      end
    end
  end
  
  def send_mommy_purchase(apt, template_index)
    email = Mailer.create_purchase_mom(apt, template_index)  
    email.set_content_type("text/html") 
    Mailer.deliver(email)
  end

  
end


# --------------------------------------------
# MAIN
# --------------------------------------------

def main(args)
    usage_and_quit if args.size == 0 or !%w(development test production).include? args[0].downcase
    
    now = Time.now
    $verbose = args.include? '-verbose'
    log("# Executing script on #{now}")
    
    # process the inbox and emails
    mommy_invite = MommyInvite.new  
    env = args[0]
    mommy_invite.process(env)
    log("Finished checker in #{Time.now-now} seconds")
end

def usage_and_quit
    puts <<-MSG    
Error: Unable to find the environment 
Usage: ruby mommy_invite.rb environment [-verbose]
  Where environment is 'development', 'test' or 'production'
  
    MSG
    exit(-1)
end

# if the script is being executed directly
if __FILE__ == $0         
  main(ARGV)
end
