<?php require_once('Connections/production.php'); ?>
<?php require_once('Connections/production.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE appointments SET start_time=%s, end_time=%s, `date`=%s, token=%s WHERE id=%s AND cancelled=%s",
                       GetSQLValueString($_POST['textfield'], "text"),
                       GetSQLValueString($_POST['textfield2'], "text"),
                       GetSQLValueString($_POST['textfield3'], "text"),
                       GetSQLValueString($_POST['textfield4'], "text"),
                       GetSQLValueString($_POST['hiddenField'], "int"),
                       GetSQLValueString($_POST['textfield5'], "int"));

  mysql_select_db($database_production, $production);
  $Result1 = mysql_query($updateSQL, $production) or die(mysql_error());

  $updateGoTo = "useradd.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_DetailRS1 = "-1";
if (isset($_GET['recordID'])) {
  $colname_DetailRS1 = $_GET['recordID'];
}
mysql_select_db($database_production, $production);
$query_DetailRS1 = sprintf("SELECT * FROM appointments  WHERE id = %s", GetSQLValueString($colname_DetailRS1, "int"));
$DetailRS1 = mysql_query($query_DetailRS1, $production) or die(mysql_error());
$row_DetailRS1 = mysql_fetch_assoc($DetailRS1);
$totalRows_DetailRS1 = mysql_num_rows($DetailRS1);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<form id="form1" name="form1" method="POST" action="<?php echo $editFormAction; ?>">
  <table border="1" align="center">
    <tr>
      <td>id</td>
      <td><?php echo $row_DetailRS1['id']; ?>
      <input name="hiddenField" type="hidden" id="hiddenField" value="<?php echo $row_DetailRS1['id']; ?>" /></td>
    </tr>
    <tr>
      <td>user_id</td>
      <td><?php echo $row_DetailRS1['user_id']; ?> </td>
    </tr>
    <tr>
      <td>start_time</td>
      <td><label>
        <input name="textfield" type="text" id="textfield" value="<?php echo $row_DetailRS1['start_time']; ?>" />
</label>      </td>
    </tr>
    <tr>
      <td>end_time</td>
      <td><label>
        <input name="textfield2" type="text" id="textfield2" value="<?php echo $row_DetailRS1['end_time']; ?>" />
      </label></td>
    </tr>
    <tr>
      <td>date</td>
      <td><label>
        <input name="textfield3" type="text" id="textfield3" value="<?php echo $row_DetailRS1['date']; ?>" />
      </label></td>
    </tr>
    <tr>
      <td>time_zone</td>
      <td><?php echo $row_DetailRS1['time_zone']; ?> </td>
    </tr>
    <tr>
      <td>token</td>
      <td><label>
        <input name="textfield4" type="text" id="textfield4" value="<?php echo $row_DetailRS1['token']; ?>" />
      </label></td>
    </tr>
    <tr>
      <td>confirmation_code</td>
      <td><?php echo $row_DetailRS1['confirmation_code']; ?> </td>
    </tr>
    <tr>
      <td>rescheduled</td>
      <td><?php echo $row_DetailRS1['rescheduled']; ?> </td>
    </tr>
    <tr>
      <td>cancelled</td>
      <td><?php echo $row_DetailRS1['cancelled']; ?> <label>
        <input name="textfield5" type="text" id="textfield5" value="<?php echo $row_DetailRS1['cancelled']; ?>" />
      </label></td>
    </tr>
    <tr>
      <td>video_removed</td>
      <td><?php echo $row_DetailRS1['video_removed']; ?> </td>
    </tr>
    <tr>
      <td>created_on</td>
      <td><?php echo $row_DetailRS1['created_on']; ?> </td>
    </tr>
    <tr>
      <td>confirmed_on</td>
      <td><?php echo $row_DetailRS1['confirmed_on']; ?> </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><label>
        <input type="submit" name="button" id="button" value="Submit" />
      </label></td>
    </tr>
  </table>
    <input type="hidden" name="MM_update" value="form1" />
</form>
</body>
</html><?php
mysql_free_result($DetailRS1);
?>
