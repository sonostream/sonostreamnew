class View < ActiveRecord::Base

  ARCHIVED = :archived
  LIVE = :live
  VIEW_TYPES = [ARCHIVED, LIVE]                      

  belongs_to :purchase
  
  def type
    view_type.to_s
  end
  
  def view_date
    viewed_on.strftime("%Y-%m-%d %I:%M:%S %p %Z")
  end


end
