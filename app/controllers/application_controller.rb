# Filters added to this controller will be run for all controllers in the application.
# Likewise, all the methods added will be available for all controllers.
class ApplicationController < ActionController::Base

  def display_error(error)
    flash.now[:notice] = error
    render :layout => "layouts/generic", :action => 'error'
  end
  
  # --------------------------------------------

  private 
  
  def send_admin_notification(page)
    flash[:notice] = "An error occurred.  The administrator has been notifed"
    # send an email to the admin
    subject =  "Got to the /#{page} page without parameters.  Hack attempt?"
    message =  "#{request.remote_ip} hit the /#{page} page.  But did not have the\n"
    message += "correct parameters from Usaepay."
    data = params#.merge(@headers)
    send_admin_email(subject, message, "Sonostream Application", data)
  end  

  # helper method for sending admin emails with error messages
  def send_admin_email(subject, message, sent_from, data=nil) 
    msg = "#{message}"
    msg += "\n\nData:    #{data.inspect}" if data
    e = Mailer.create_admin_email(subject, msg, sent_from)              
    Mailer.deliver(e)    
  end
end