README

This is the README for Sonostream Beta.

== Dependencies ==

   will_paginate version 2.3.16

You must install to the /usr/lib/ruby/gems/1.8 directory.  You can't install to the default /usr/local/lib; passenger doesn't pick it up.  

    gem install will_paginate -v 2.3.16 --install-dir /usr/lib/ruby/gems/1.8

