
module Constants

  # todo: this regex doesn't work with emails with a single letter domain.  i.e. x@x.com
  EMAIL_REGEX = /^[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]*@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$/
  # matches an email, or a blank.  see | at the end
  EMAIL_REGEX_OR_BLANK = /^[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$|$^/
  NAME_REGEX = /^[-`'a-zA-Z0-9_.\s]+$/
  URL_REGEX = /^$|^http:\/\/.*$/
  INVALID_CHARS = /[^-a-zA-Z0-9_.]/
  INVALID_CHARACTER_MSG = "has invalid characters.  A - Z, a - z, /, -, 0-9, _, ., and spaces, only."                      
  TRUE_FALSE_REGEX = /^[01]$/

  def login
     self.class.find(:first, :conditions  => ["email = ? and password = ?", email.downcase, password])
  end

end